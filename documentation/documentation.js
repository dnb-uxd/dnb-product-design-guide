jQuery(function($) {

  // Wait for scroll to end, then do something
  $.fn.scrollEnd = function(callback, timeout) {
    $(this).scroll(function(){
      var $this = $(this);
      if ($this.data('scrollTimeout')) {
        clearTimeout($this.data('scrollTimeout'));
      }
      $this.data('scrollTimeout', setTimeout(callback,timeout));
    });
  };


  // don't do anything for links with href="#"
  $("a[href^=#]").click(function(e) {
    e.preventDefault();
  });


  // reset position cookie on nav link click
  $("#globalNav a, .getting-started a[href^='/documentation/components/']").click(function(e) {
    $.cookie('scrollPosition', 0);
    $.cookie('hasScrolled', 0);
  });

  // Datatables
  $('#exampleTable1, #exampleTable2, #exampleTable3').dataTable({
    "paging":   false,
    "info":     false,
    "searching":   false,
    "dom": 'Rlfrtip'
  });
    // Datatables for sorting
  $('#exampleTable4').dataTable({
    "paging":   false,
    "info":     false,
    "searching":   false,
    "autoWidth": true,
    "dom": '<"scroll-table"flipt>'
  });

  // Header demos
  $('.header-demos .dnb-global-header .navbar-toggle').click(function() {
    $(this).parents('.dnb-global-header').toggleClass('demo-active');
  })



  // Carousel demo
  $('.carousel-indicators li').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
  })

  // Initialize tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Initialize popovers
  $('[data-toggle="popover"]').popover({container: '.main-body'});


  //
  // Left-hand menu stuff
  //

  // Open/Close menus and go to section
  $('.dnb-left-hand-menu').on('click', 'a', function(e) {
    var $this = $(this);
    var href = $($this.attr('href'));

    $('.dnb-left-hand-menu').removeClass('scrollSpy-active');

    if ($this.hasClass('accordion-toggle')) {
      setTimeout(function() {
        $this.blur();
      }, 250);
    } else {
      $('.open').removeClass('withActive');
      $this.parents('.dnb-left-hand-menu').find('.active').removeClass('active');

      $this.parent().addClass('active');

      if ($('.active').parents('.open').length) {
        $('.active').parents('.open').addClass('withActive');
      }

      if ($('body').hasClass('has-sidebar') && $('body').hasClass('header-fixed')) {
        $('.page-wrapper').animate({
          scrollTop: href.position().top + 41
        });
      } else {
        $('body').animate({
          scrollTop: href.offset().top - $('.dnb-global-header').outerHeight() + 10
        });
      }
    }
    
    setTimeout(function() {
      $('.dnb-left-hand-menu').addClass('scrollSpy-active');
      $this.blur();
    }, 500);
  });

  // Populate the left-hand-menu
  var componentsList = [];
  var leftNav = $('.dnb-left-hand-menu ul');
  var directoryList = "directory.json";
  $.getJSON( directoryList, function( directory ) {
    $.each(directory, function(key, val) {
      var parent, dropdown, component, item;
      if (typeof val == 'string') {
        if (val.indexOf('page_') == -1 || val.indexOf('page_loader') > -1) {
          component = val.substring(0, val.indexOf('.'));
          var componentDisplayName = component.replace(/_/g, " ");
          if (component.indexOf('whats') > -1) {
            componentDisplayName = componentDisplayName.replace("whats", "what's");
          }
          item = '<li role="presentation"><a href="#' + component + '">' + componentDisplayName + '</a></li>' ;
          leftNav.append(item);
        }
      } else {
        dropdown = $('<ul class="accordion-menu" role="menu"></ul>');
        parent = $('<li role="presentation"><a class="accordion-toggle" data-toggle="accordion" href="#" role="button" aria-expanded="false"">' + key.replace(/_/g, " ") + ' <span class="caret"></span></a></li>');
        parent.append(dropdown);
        $.each(val, function(childKey, childVal) {
          if (childVal.indexOf('page_') == -1) {
            component = childKey.substring(0, childKey.indexOf('.'));
            var componentDisplayName = component.replace(/_/g, " ");
            if (childVal.indexOf('faq') > -1) {
              componentDisplayName = componentDisplayName + '?';
            }
            item = '<li role="presentation"><a href="#' + component + '">' + componentDisplayName + '</a></li>' ;
            dropdown.append(item);
          }
        });
        leftNav.append(parent);
      }
    });
    setTimeout(function() {
      startScrollSpy();

      // Scroll to the right position on page load
      if ($.cookie('scrollPosition') == 0) {
        var hash = $(window.location.hash);
        if (hash.length) {
          $('.page-wrapper').scrollTop(hash.position().top +41);
        }
        fromGettingStarted = 0;
      } else {
        $('.page-wrapper').scrollTop($.cookie('scrollPosition'));
      }
      $('.page-wrapper > div > div').css('visibility', 'visible');
      $('.amp-loader').hide();
    },  70);
  });


  // Start ScrollSpy
  function startScrollSpy() {
    if ($('body').hasClass('has-sidebar') && $('body').hasClass('header-fixed')) {
      $('.page-wrapper').scrollspy({ 
        target: '.scrollSpy-active',
        offset: -40
      });
    } else {
      $('body').scrollspy({ 
        target: '.scrollSpy-active',
        offset: 71
      });
    }
  }


  // Open menu items on scrollspy
  $('.dnb-left-hand-menu').on('activate.bs.scrollspy', function () {
    if (!$('.dnb-left-hand-menu .withActive').find('.active').length) {
      if ($('.dnb-left-hand-menu .withActive').hasClass('manual')) {
        $('.dnb-left-hand-menu .withActive').removeClass('withActive');
      } else {
        $('.dnb-left-hand-menu .withActive .accordion-menu').slideUp(400);
        $('.dnb-left-hand-menu .withActive').removeClass('open withActive');
      }
    }

    $('.dnb-left-hand-menu .active').each(function(key, elem) {
      if ($(elem).children('.accordion-menu').length) {
        $(elem).removeClass('active');
        if (!$(elem).hasClass('open')) {
          $(elem).addClass('open');
          $(elem).children('.accordion-menu').slideDown(400);
          $(elem).removeClass('active').addClass('withActive');
        } else {
          $(elem).removeClass('active').addClass('withActive');
        }
      }
    });
  });

  $(window).scroll(function() {
    if ($('body').scrollTop() < 410){
      if (!$('.dnb-left-hand-menu .withActive').find('.active').length) {
        if ($('.dnb-left-hand-menu .withActive').hasClass('manual')) {
          $('.dnb-left-hand-menu .withActive').removeClass('withActive');
        } else {
          $('.dnb-left-hand-menu .withActive .accordion-menu').slideUp(400);
          $('.dnb-left-hand-menu .withActive').removeClass('open withActive');
        }
      }
    }
  });

  // Page position when page has fixed header and sidebar
  if ($('body').hasClass('header-fixed') && $('body').hasClass('has-sidebar')) {

    // Set cookie with scroll position
    $('.page-wrapper').scrollEnd(function(){
      $.cookie('scrollPosition', $('.page-wrapper ').scrollTop());
      $.cookie('hasScrolled', 1);
    }, 250);

    // Left-hand menu behavior
    $('.page-wrapper').scroll(function() {
      // console.log($('.page-wrapper > div').scrollTop())
      if ($('.page-wrapper').scrollTop() < 410){
        if (!$('.dnb-left-hand-menu .withActive').find('.active').length) {
          if ($('.dnb-left-hand-menu .withActive').hasClass('manual')) {
            $('.dnb-left-hand-menu .withActive').removeClass('withActive');
          } else {
            $('.dnb-left-hand-menu .withActive .accordion-menu').slideUp(400);
            $('.dnb-left-hand-menu .withActive').removeClass('open withActive');
          }
        }
      }
    });
  }


  // Make things the same height, like widgets or whaterver you want
  function colHeight(cols) {
    var tallest = 0;
    
    $(cols).each(function() {
      var thisHeight = $(this).outerHeight();
      
      if (thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    $(cols).outerHeight(tallest);
  }
  // Make things the same min-height
  function colMinHeight(cols) {
    var tallest = 0;
    
    $(cols).each(function() {
      var thisHeight = $(this).outerHeight();
      
      if (thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    $(cols).css('min-height', tallest);
  }

  //
  // Make swatchtags the same height;
  //
  colHeight('.primary-colors > div');
  colHeight('.secondary-colors > div');
  colHeight('.amp-loader-example');

  // Toggle validation states
  $('.toggle-states').click(function(e) {
    var states = ['has-success', 'has-warning', 'has-error']
    e.preventDefault();

    if ($(this).siblings().find('.has-success').length) {
      $(this).siblings().find('.form-group').removeClass('has-success has-warning has-error');
    } else {
      $(this).siblings().find('.form-group').each(function(key, val) {
        $(this).addClass(states[key]);
      });
    }


    $(this).blur();
  });

  // Toggle label for toggle-switches
  $('.btn-toggle input').on('change', function() {
    if ($(this).is(':checked')) {
      // console.log($(this).parents('.toggle-group').length)
      $(this).parents('.toggle-group').next().html('On');
    } else {
      $(this).parents('.toggle-group').next().html('Off');
    }
  })

  //
  // Make code in <pre> and <code> pretty
  //
  prettyPrint();




});

// $.fn.dataTableExt.oStdClasses.sScrollWrapper = "table-scroll";
// $.fn.dataTableExt.oStdClasses.sScrollHead = "table-scrollHead";
// $.fn.dataTableExt.oStdClasses.sScrollHeadInner = "table-scrollHeadInner";
// $.fn.dataTableExt.oStdClasses.sScrollBody = "table-scrollBody";
// $.fn.dataTableExt.oStdClasses.sScrollFoot = "table-scrollFoot";
// $.fn.dataTableExt.oStdClasses.sScrollFootInner = "table-scrollFootInner";
// $.fn.dataTableExt.oStdClasses.sLength = "table-length";
// $.fn.dataTableExt.oStdClasses.sPaging = "table-paginate paging_";
