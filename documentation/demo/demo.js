$(function() {

  $("a[href*=#]").click(function(e) {
    e.preventDefault();
  });


  $('.dnb-left-hand-menu').on('click', 'a', function(e) {
    var $this = $(this);
    var href = $($this.attr('href'));

    e.preventDefault();

    if ($this.hasClass('accordion-toggle')) {
      setTimeout(function() {
        $this.blur();
      }, 250);
    } else {
      $('.open').removeClass('withActive');
      $this.parents('#left-hand-menu').find('.active').removeClass('active');

      $this.parent().addClass('active');

      if ($('.active').parents('.open').length) {
        $('.active').parents('.open').addClass('withActive');
      }

      $('body').animate({
        scrollTop: href.offset().top - $('.dnb-global-header').outerHeight() + 10
      });
    }
  });




  // Datatables
  $('#exampleTable1').dataTable({
    "paging":   false,
    "info":     false,
    "searching":   false,
    "dom": 'Rlfrtip',
    "order": [[ 1, "asc" ]]
  });
  $('#exampleTable4').dataTable({
    "scrollY": 340,
    "scrollX": true,
    "paging":   false,
    "info":     false,
    "searching":   false,
    "initComplete": function() {
      // Add class for styling
      if ($(this).width() > $(this).parent().width()) {
        $(this).parents('.table-scroll').addClass('scroll-horiz');
      }
      if ($(this).outerHeight() > $(this).parent().outerHeight()) {
        $(this).parents('.table-scroll').addClass('scroll-vert');
      }
      if ($('.dataTables_sizing').length) {
        $('.dataTables_sizing').removeClass('dataTables_sizing').addClass('table-sizing');
      }
    }
  });



  // Make things the same height, like widgets or whaterver you want
  function colHeight(cols) {
    var tallest = 0;
    
    $(cols).each(function() {
      var thisHeight = $(this).outerHeight();
      
      if (thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    $(cols).outerHeight(tallest);
  }
  // Make things the same min-height
  function colMinHeight(cols) {
    var tallest = 0;
    
    $(cols).each(function() {
      var thisHeight = $(this).outerHeight();
      
      if (thisHeight > tallest) {
        tallest = thisHeight;
      }
    });
    $(cols).css('min-height', tallest);
  }


  if ($(window).width() > 768) {
    colHeight('.sameheight .panel-body');
  }


  $(window).resize(function() {
    if ($(window).width() > 991) {
      colHeight('.sameheight .panel-body');
    } else {
      $('.sameheight .panel-body').height('auto');
    }
  });



});

$.fn.dataTableExt.oStdClasses.sScrollWrapper = "table-scroll";
  $.fn.dataTableExt.oStdClasses.sScrollHead = "table-scrollHead";
  $.fn.dataTableExt.oStdClasses.sScrollHeadInner = "table-scrollHeadInner";
  $.fn.dataTableExt.oStdClasses.sScrollBody = "table-scrollBody";
  $.fn.dataTableExt.oStdClasses.sScrollFoot = "table-scrollFoot";
  $.fn.dataTableExt.oStdClasses.sScrollFootInner = "table-scrollFootInner";
  $.fn.dataTableExt.oStdClasses.sLength = "table-length";
  $.fn.dataTableExt.oStdClasses.sPaging = "table-paginate paging_";