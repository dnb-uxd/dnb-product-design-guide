'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var util = require('gulp-util');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var header = require('gulp-header');
var zip = require('gulp-zip');
var directoryMap = require("gulp-directory-map");
var markdown = require('gulp-markdown');
var toMarkdown = require('gulp-to-markdown');

var pkg = require('./package.json');
var banner = ['/**',
  ' * <%= pkg.name %> v<%= pkg.version %> (<%= pkg.homepage %>)',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');

var LessPluginCleanCSS = require('less-plugin-clean-css'),
    cleancss = new LessPluginCleanCSS({advanced: true});

var LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    autoprefix = new LessPluginAutoPrefix({browsers: ['last 3 versions']});

gulp.task('connect', ['watch'], function() {
  connect.server({
    livereload: true
  });
});

// Compiles less > css (and creates sourcemap)
gulp.task('less', function(){
    return gulp.src('./less/dnb-product-design-guide.less')
    .pipe(sourcemaps.init())
    .pipe(less().on('error', util.log))
    .pipe(rename('dnb-product-design-guide.css'))
    //.pipe(autoprefixer())
    .pipe(sourcemaps.write()).on('error', util.log)
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(gulp.dest('./dist/css'));
});

// Minify css
gulp.task('minify', ['less'], function() {
    return gulp.src('./dist/css/dnb-product-design-guide.css')
    .pipe(less({
        plugins: [cleancss]
    }).on('error', util.log))
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(rename('dnb-product-design-guide.min.css'))
    .pipe(gulp.dest('./dist/css'))
    .pipe(connect.reload());
});

// Watch files
gulp.task('watch', function () {
  gulp.watch(['less/*.less','less/**/*.less'], ['minify', 'docs-less']);
  gulp.watch('js/**/*.js', ['compress']);
  gulp.watch('documentation/*.js', ['docs-js']);
  gulp.watch('documentation/*.less', ['docs-less']);
  gulp.watch('documentation/src/**/*.html', ['docs']);
  gulp.watch('documentation/demo/*.html', ['demos']);
});

// Add version number to top of files
gulp.task('version', function() {
  gulp.src('js/dnb-dev/*.js')
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(gulp.dest('./js'))
});

// Concat javascript
gulp.task('scripts',['version'], function() {
  return gulp.src([
      'js/transition.js',
      'js/alert.js',
      'js/button.js',
      'js/carousel.js',
      'js/collapse.js',
      'js/dropdown.js',
      'js/modal.js',
      'js/tooltip.js',
      'js/popover.js',
      'js/scrollspy.js',
      'js/tab.js',
      'js/affix.js',
      'js/global-search.js',
      'js/global-left-hand-menu.js',
      'js/input-file.js',
      'js/scrollable-tables.js',
      'js/dnb-popover.js'
    ])
    .pipe(concat('dnb-product-design-guide.js'))
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('compress', ['scripts'], function() {
  gulp.src('./dist/js/dnb-product-design-guide.js')
    .pipe(uglify())
    .pipe(header(banner, { pkg : pkg } ))
    .pipe(rename('dnb-product-design-guide.min.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(connect.reload());
});

gulp.task('zip', function () {
  gulp.src('./dist/*')
    .pipe(zip('dnb-product-design-guide-' + pkg.version + '-dist.zip'))
    .pipe(gulp.dest('./builds'));
  gulp.src(['./*', '!./dist'])
    .pipe(zip('dnb-product-design-guide-' + pkg.version + '-src.zip'))
    .pipe(gulp.dest('./builds'));
});


// Default generates css, source maps, minifies, and moves fonts to dist
gulp.task('default', ['minify', 'compress', 'zip', 'to-markdown', 'docs']);  // 'fonts'







//
// Documentation
//


gulp.task('to-markdown', function () {
    return gulp.src([
      './documentation/src/getting-started/overview.html',
      './documentation/src/getting-started/download.html',
      './documentation/src/getting-started/whats_included.html',
      './documentation/src/getting-started/documentation.html'
      ])
        .pipe(toMarkdown())
        .pipe(concat('./README.md'))
        .pipe(gulp.dest('./'));
});
// Compiles demo less > css
gulp.task('docs-less', function(){
    return gulp.src('documentation/*.less')
    .pipe(less().on('error', util.log))
    .pipe(rename('documentation.css'))
    .pipe(gulp.dest('./documentation'))
    .pipe(connect.reload());
});
// Reload docs js
gulp.task('docs-js', function(){
    return gulp.src('documentation/*.js')
    .pipe(connect.reload());
});

gulp.task('docs', function() {
  var gettingStartedSrc = [
      // Page Header- exclude from directory map
      'documentation/src/getting-started/page_helpers/page_wrap_head.html',
      // Demo Controls
      'documentation/src/getting-started/page_helpers/page_header.html',
      'documentation/src/getting-started/page_helpers/page_left-hand_menu.html',
      // Page Wrapper - exclude from directory map
      'documentation/src/getting-started/page_helpers/page_wrap_begin.html',
      // Overview
      'documentation/src/getting-started/overview.html',
      // Install
      'documentation/src/getting-started/download.html',
      // What's Included
      'documentation/src/getting-started/whats_included.html',
      // Basic Usage
      'documentation/src/getting-started/basic_usage.html',
      // Demo
      'documentation/src/getting-started/demo.html',
      // FAQ
      'documentation/src/getting-started/faq/faq_page_heading.html',
      'documentation/src/getting-started/**/why_bootstrap.html',
      'documentation/src/getting-started/**/why_a_fork.html',
      'documentation/src/getting-started/**/how_do_I_customize.html',
      'documentation/src/getting-started/**/maintenance.html',
      'documentation/src/getting-started/**/how_do_I_stay_up_to_date.html',

      'documentation/src/getting-started/faq/faq_page_footer.html',

      // Page Wrapper - exclude from directory map
      'documentation/src/getting-started/page_helpers/page_wrap_end.html'
  ];
  var componentsSrc = [
      // Page Header- exclude from directory map
      'documentation/src/components/page_helpers/page_wrap_head.html',
      // Demo Controls
      'documentation/src/components/page_helpers/page_header.html',
      'documentation/src/components/page_helpers/page_left-hand_menu.html',
      // Page Wrapper - exclude from directory map
      'documentation/src/components/page_helpers/page_wrap_begin.html',
      // Overview
      'documentation/src/components/overview.html',
      // Global stuff
      'documentation/src/components/global_header.html',
      'documentation/src/components/left-hand_menu.html',
      // Colors
      'documentation/src/components/**/primary.html',
      'documentation/src/components/**/secondary.html',
      'documentation/src/components/**/risk.html',
      'documentation/src/components/**/charts.html',
      'documentation/src/components/**/common_usage.html',
      // Typography
      'documentation/src/components/typography.html',
      // Icons
      'documentation/src/components/icons.html',
      // Buttons
      'documentation/src/components/**/colors.html',
      'documentation/src/components/**/disabled.html',
      'documentation/src/components/**/sizes.html',
      'documentation/src/components/**/variations.html',
      // Panels
      'documentation/src/components/panels.html',
      // Cards
      'documentation/src/components/**/gray_bg.html',
      'documentation/src/components/**/white_bg.html',
      // Accordions
      'documentation/src/components/accordions.html',
      // tables
      'documentation/src/components/**/basic_table_usage.html',
      'documentation/src/components/**/hover_rows.html',
      'documentation/src/components/**/contextual_classes.html',
      'documentation/src/components/**/scrollable_tables.html',
      // breadcrumbs
      'documentation/src/components/breadcrumbs.html',
      // pagination
      'documentation/src/components/pagination.html',
      // tabs
      'documentation/src/components/tabs.html',
      // forms
      'documentation/src/components/**/basic_form_usage.html',
      'documentation/src/components/**/disabled_state.html',
      'documentation/src/components/**/validation_states.html',
      'documentation/src/components/**/checkboxes_and_radios.html',
      'documentation/src/components/**/toggle_switch.html',
      // alerts
      'documentation/src/components/alerts.html',
      // notices
      'documentation/src/components/notices.html',
      // labels
      'documentation/src/components/labels.html',
      // tooltips
      'documentation/src/components/tooltips.html',
      // popovers
      'documentation/src/components/popovers.html',
      // progress-bars
      'documentation/src/components/progress_bars.html',
      // modals
      'documentation/src/components/modals.html',
      // page_loader
      'documentation/src/components/page_loader.html',
      // carousel-dots
      'documentation/src/components/carousel_dots.html',
      // Helper Classes
      'documentation/src/components/**/bootstrap_helpers.html',
      'documentation/src/components/**/DB_helpers.html',
      // Footers
      'documentation/src/components/**/login.html',
      'documentation/src/components/**/product.html',
      'documentation/src/components/**/marketing.html',
      // Page Wrapper - exclude from directory map
      'documentation/src/components/page_helpers/page_wrap_end.html'
    ];
  gulp.src(gettingStartedSrc)
    .pipe(directoryMap({
      filename: 'directory.json'
    }))
    .pipe(gulp.dest('./documentation'));
  gulp.src(gettingStartedSrc)
    .pipe(concat('index.html'))
    .pipe(gulp.dest('./documentation'));
  gulp.src(componentsSrc)
    .pipe(directoryMap({
      filename: 'directory.json'
    }))
    .pipe(gulp.dest('./documentation/components'));
  gulp.src(componentsSrc)
    .pipe(concat('index.html'))
    .pipe(gulp.dest('./documentation/components'))
    .pipe(connect.reload());
});



// Reload demo pages
gulp.task('demos', function(){
  return gulp.src('documentation/demo/*.html')
    .pipe(connect.reload());
});




gulp.task('wp-less', function() {
  gulp.src('dist/css/*.css')
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/library/dnb-product-design-guide/css'));
});

gulp.task('wp-docs', function() {
  var componentsSrc = [
      // Page Header- exclude from directory map
      'documentation/src/components/page_helpers/page_wp_header.html',
      // Demo Controls
      //'documentation/src/components/page_helpers/page_header.html',
      'documentation/src/components/page_helpers/page_left-hand_menu.html',
      // Page Wrapper - exclude from directory map
      'documentation/src/components/page_helpers/page_wrap_begin.html',
      // Overview
      //'documentation/src/components/overview.html',
      // Overview
      'documentation/src/getting-started/overview.html',
      // Install
      'documentation/src/getting-started/download.html',
      // What's Included
      'documentation/src/getting-started/whats_included.html',
      // Basic Usage
      'documentation/src/getting-started/basic_usage.html',
      // Demo
      'documentation/src/getting-started/demo.html',
      // Global stuff
      'documentation/src/components/global_header.html',
      'documentation/src/components/left-hand_menu.html',
      // Colors
      'documentation/src/components/**/primary.html',
      'documentation/src/components/**/secondary.html',
      'documentation/src/components/**/risk.html',
      'documentation/src/components/**/charts.html',
      'documentation/src/components/**/common_usage.html',
      // Typography
      'documentation/src/components/typography.html',
      // Icons
      'documentation/src/components/icons.html',
      // Buttons
      'documentation/src/components/**/colors.html',
      'documentation/src/components/**/disabled.html',
      'documentation/src/components/**/sizes.html',
      'documentation/src/components/**/variations.html',
      // Panels
      'documentation/src/components/panels.html',
      // Cards
      'documentation/src/components/**/gray_bg.html',
      'documentation/src/components/**/white_bg.html',
      // Accordions
      'documentation/src/components/accordions.html',
      // tables
      'documentation/src/components/**/basic_table_usage.html',
      'documentation/src/components/**/hover_rows.html',
      'documentation/src/components/**/contextual_classes.html',
      'documentation/src/components/**/scrollable_tables.html',
      // breadcrumbs
      'documentation/src/components/breadcrumbs.html',
      // pagination
      'documentation/src/components/pagination.html',
      // tabs
      'documentation/src/components/tabs.html',
      // forms
      'documentation/src/components/**/basic_form_usage.html',
      'documentation/src/components/**/disabled_state.html',
      'documentation/src/components/**/validation_states.html',
      'documentation/src/components/**/checkboxes_and_radios.html',
      'documentation/src/components/**/toggle_switch.html',
      // alerts
      'documentation/src/components/alerts.html',
      // notices
      'documentation/src/components/notices.html',
      // labels
      'documentation/src/components/labels.html',
      // tooltips
      'documentation/src/components/tooltips.html',
      // popovers
      'documentation/src/components/popovers.html',
      // progress-bars
      'documentation/src/components/progress_bars.html',
      // modals
      'documentation/src/components/modals.html',
      // page_loader
      'documentation/src/components/page_loader.html',
      // carousel-dots
      'documentation/src/components/carousel_dots.html',
      // Helper Classes
      'documentation/src/components/**/bootstrap_helpers.html',
      'documentation/src/components/**/DB_helpers.html',
      // Footers
      'documentation/src/components/**/login.html',
      'documentation/src/components/**/product.html',
      'documentation/src/components/**/marketing.html',
      // FAQ
      'documentation/src/getting-started/faq/faq_page_heading.html',
      'documentation/src/getting-started/**/why_bootstrap.html',
      'documentation/src/getting-started/**/why_a_fork.html',
      'documentation/src/getting-started/**/how_do_I_customize.html',
      'documentation/src/getting-started/**/maintenance.html',
      'documentation/src/getting-started/**/how_do_I_stay_up_to_date.html',
      'documentation/src/getting-started/faq/faq_page_footer.html',
      // Page Wrapper - exclude from directory map
      'documentation/src/components/page_helpers/page_wp_footer.html'
    ];
  gulp.src(componentsSrc)
    .pipe(directoryMap({
      filename: 'directory.json'
    }))
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/documentation'));
  gulp.src(componentsSrc)
    .pipe(concat('page-components.php'))
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/'));
  gulp.src(['documentation/documentation.js', 'documentation/jquery.cookie.js'])
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/documentation'));
  gulp.src('dist/css/*.css')
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/library/dnb-product-design-guide/css'));
  gulp.src('dist/js/*.js')
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/library/dnb-product-design-guide/js'));
  gulp.src(['dist/img/*.gif', 'dist/img/*.png'])
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/library/dnb-product-design-guide/img'));
  gulp.src(['documentation/img/*.jpg', 'documentation/img/dnb-product-design-logo-footer.png'])
    .pipe(gulp.dest('../wp/wp-content/themes/dnb-product-style-guide/library/images'));
});
