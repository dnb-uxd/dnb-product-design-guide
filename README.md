# D&B Product Design Guide

The D&B Product Design Guide created to make your job easier! It includes everything you need to quickly recreate any of the examples found on the [Product Design Guide](http://productdesign.dnb.com/) website.


##### Current Version: 1.0.0


These components are a customization of [Bootstrap](http://getbootstrap.com) and the items provided here are intended to be used in lieu of Bootstrap. You can follow the guidlines set by Bootstrap to create the elements and layouts you need.  Any nuances added by the D&B UXD team are noted on the [Product Design Guide Documentation](http://uxd-staging.com/product-style-guide/documentation/) page with sample code snippets.

New components are being added regularly, so make sure you have the latest version.  If you need something that has not yet been added, please contact [Cassie Sharp](mailto:csharp@hoovers.com?Subject=Product%20Design%20-%20Design%20Inquiry) and we will try to get it added as quickly as possible.


## Download


There are a few ways to get started:

### Precompiled Code

Preompiled and minified CSS, JavaScript, and global images. No docs or original source files are included.  Link up the files of your choice in your `<head>` and you're good to go.

*   [Download the latest precompiled release](https://bitbucket.org/dnb-uxd/dnb-product-design-guide/downloads/dnb-product-design-guide-1.0.0-dist.zip)

### Source Code

Source Less, JavaScript, and basic images, along with our docs. Requires a Less compiler.  Setup after download will depend on how you plan on using it.

*   [Download the latest release source code](https://bitbucket.org/dnb-uxd/dnb-product-design-guide/downloads/dnb-product-design-guide-1.0.0-src.zip)
*   Clone Repo: `git clone https://bitbucket.org/dnb-uxd/dnb-product-design-guide.git`
*   Install with [npm](https://www.npmjs.com/): `npm install dnb-product-design-guide`

## What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations.

### Precompiled Code

Once downloaded, unzip the compressed folder to see the structure of (the compiled) Product Design Guide. You'll see something like this:


    dnb-product-design-guide/
      ├── css/
      │   ├── dnb-product-design-guide.css
      │   └── dnb-product-design-guide.min.css
      ├── img/
      │   └── General D&B Images
      └── js/
             ├── dnb-product-design-guide.js
             └── dnb-product-design-guide.min.js


We provide compiled CSS and JS (`dnb-product-design-guide.*`), as well as compiled and minified CSS and JS (`dnb-product-design-guide.min.*`). CSS source maps are built into `dnb-product-design-guide.css` and are available for use with certain browsers' developer tools. We also provide the source code for LESS and javascript, in case you need to customize something for your specific app and to give you access to the LESS variables.

_Custom icon fonts are imported into the CSS and do not require anything to set up._

### Source Code

The D&B Product Design Guide source code download includes the precompiled CSS, JavaScript, and image assets, along with source Less, JavaScript, and documentation. More specifically, it includes the following and more:


      dnb-product-design-guide/
      ├── less/
      ├── js/
      ├── dist/
      │   ├── css/
      │   ├── js/
      │   └── img/
      └── documentation/
          └── demo/


The `less/` and `js/` are the source code for our CSS and JS (respectively). The `dist/` folder includes everything listed in the precompiled download section above. The `documentation/` folder includes the source code for our documentation, and `demo/` includes a few examples of D&B Product Design Guide usage. Beyond that, any other included file provides support for packages, license information, and development.

_Custom icon fonts are imported into the CSS and do not require anything to set up._

## Documentation

D&B Product Design Guide documentation, included in this repo in the root directory, is publicly hosted at [http://uxd-staging.com/product-style-guide/documentation/](http://uxd-staging.com/product-style-guide/documentation/). The docs may also be run locally.

---

Designed, coded, and maintained (with love) by the [D&B UXD Team](http://design.dnb.com/).  For questions or additional help, contact [Haidy Francis](mailto:Perez-FrancisH@DNB.com).