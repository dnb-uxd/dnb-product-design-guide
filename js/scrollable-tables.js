/**
 * dnb-product-design-guide v1.0.0 (http://productdesign.dnb.com/)
 * @license ISC
 */
/* ========================================================================
 * D&B Product Styles: scrollable-tables.js
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  'use strict';
  if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
    $('html').addClass('firefox');
  }

  // Give the page a few ms to load the table
  setTimeout(function() {
    $('.scroll-table-wrapper').each(function() {
      var scrollTable = $(this);
      var scrollTableBodyHeight = 0;
      scrollTable.find('th').each(function(thKey, thVal) {
        var thWidth = $(this).width();
        $(this).width(thWidth);
      });
      scrollTable.find('tbody tr').each(function() {
        $(this).find('td').each(function(tdKey, thVal) {
          $(this).width($(scrollTable.find('th')[tdKey]).width());
        });
      })

      scrollTable.find('tbody tr').each(function() {
        scrollTableBodyHeight += $(this).outerHeight();
      });
      if (scrollTable.find('.table').width() > scrollTable.width()) {
        scrollTable.addClass('scroll-horiz');
      }
      if ( scrollTableBodyHeight > $('.scroll-table table').data('table-height') - $('.scroll-table thead tr').outerHeight()) {
        if (scrollTable.find('.old_ie_wrapper').length) {
          scrollTable.find('.old_ie_wrapper').outerHeight($('.scroll-table table').data('table-height'));
          scrollTable.addClass('no-shadow');
        } else {
          if ($('html').hasClass('firefox')) {
            scrollTable.find('tbody').outerHeight($('.scroll-table table').data('table-height'));
          } else {
            scrollTable.find('tbody').outerHeight($('.scroll-table table').data('table-height') - 21);
          }
          scrollTable.height($('.scroll-table table').data('table-height') + scrollTable.find('thead tr').outerHeight())
        }
        scrollTable.addClass('scroll-vert');
      }
    });
  }, 200);
});