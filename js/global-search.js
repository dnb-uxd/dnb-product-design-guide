/**
 * dnb-product-design-guide v1.0.0 (http://productdesign.dnb.com/)
 * @license ISC
 */
/* ========================================================================
 * D&B Product Design: global-search.js
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  
  'use strict';

  // Wait for scroll to end, then do something
  $.fn.scrollEnd = function(callback, timeout) {
    $(this).scroll(function(){
      var $this = $(this);
      if ($this.data('scrollTimeout')) {
        clearTimeout($this.data('scrollTimeout'));
      }
      $this.data('scrollTimeout', setTimeout(callback,timeout));
    });
  };

  // Wait for resize to end, then do something
  $.fn.resizeEnd = function(callback, timeout) {
    $(this).resize(function(){
      var $this = $(this);
      if ($this.data('resizeTimeout')) {
        clearTimeout($this.data('resizeTimeout'));
      }
      $this.data('resizeTimeout', setTimeout(callback,timeout));
    });
  };

  var navBar = $('.dnb-global-header');
  var navBarHeader = $('.dnb-global-header .navbar-header');
  var searchBar = navBar.find('.search-group');
  var searchForm = $('.search-group .navbar-form');

  var selector = $('.search-selector .dropdown-menu');
  var mFirstChild = selector.children('li:first-child');
  var mLastChild = selector.children('li:last-child');
  var closestOffset = 0;
  var selectorItems = [];

  var selectorWidth;
  var selectorCenter;

  var paddingSet = 0;
  var readyToToggle = 1;
  var isMobile = 0;


  function checkGlobalSearchField() {
    var navLogoWidth = navBar.find('.navbar-brand').outerWidth();
    var navbarNavWidth = $('.navbar-nav').width();

    if ($(window).width() < 768) {
      selectorWidth = selector.outerWidth();
      selectorCenter = selectorWidth / 2;
      paddingSet = 0;
      
      navBarHeader.width("auto");
      searchBar.width(navBarHeader.width() - navLogoWidth - $('.navbar-toggle').outerWidth() - 30);
    } else {
      searchForm.height("auto").width('100%').show();
      navBarHeader.outerWidth(navBar.width() - navbarNavWidth - 45);
      searchBar.width(navBarHeader.width() - navLogoWidth - 15);
      setTimeout(function() {
        navBar.removeClass('searchFocus');
      }, 250);
    }
  }

  // Set size of Global header nav search field
  setTimeout(function() {
    checkGlobalSearchField();
  }, 10);

  // Do stuff when the window resizes
  $(window).resize(function() {
    checkGlobalSearchField();
    if ($(window).width() < 768) {
      setScrollPadding();
      searchForm.height($(window).height() - navBar.height());
      // Just do this stuff once when switching from 
      // desktop to mobile view
      if (!isMobile) {
        // Hide the desktop search dropdown
        if ($('.search-selector').hasClass('open')) {
          $('.btn-navbar-search-dropdown').click();
        }
        // If the mobile search dropdown is visible, 
        // make sure the body has the .no-scroll class
        if (searchBar.hasClass('in')) {
          $('body').addClass('no-scroll');
        } else {
          searchForm.hide();
        }
        isMobile = 1;
      }
    } else {
      // Just do this stuff once when switching from 
      // mobile to desktop view
      if (isMobile) {
        // Remove the .no-scroll class from body for desktop
        if (searchBar.hasClass('in')) {
          $('body').removeClass('no-scroll')
        }
        isMobile = 0;
      }
    }
  });

  //
  // Do this stuff when the page loads
  //
  if ($(window).width() < 768) {
    // Reset stuff for mobile
    checkGlobalSearchField();
    isMobile = 1;

  } else {
    // Desktop functionality
    isMobile = 0;
  }



  //
  // Global header nav search field expanding
  //
  $('.dnb-global-header .search.form-control').focus(function() {
    if (!isMobile) {
      var navBar = $(this).parents('.navbar-default');
      var navLogoWidth = navBar.find('.navbar-brand').outerWidth();
      var searchNewWidth;
      var focusPlaceholder = $(this).data("focus-placeholder");

      searchBar.css({
        'width': searchBar.width(),
        'left': navLogoWidth  
      });
      navBar.addClass('searchFocus');
      searchNewWidth = navBar.width() - navLogoWidth - ($('.searchFocus .utility-btn').width() * $('.searchFocus .utility-btn').length) - 40;
      if ($('.searchFocus .utility-btn').length) {
        searchNewWidth -= 20;
      }
      setTimeout(function() {
        searchBar.width(searchNewWidth);
      }, 10);

      // Change placeholder to nice message on focus
      $(this).attr("placeholder", focusPlaceholder);  
    }
  });

  $('.search.form-control').blur(function() {
    if (!isMobile) {
      var origPlaceholder = $(this).data("no-focus-placeholder");

      // Revert placeholder back to "Search..."
      $(this).attr("placeholder", origPlaceholder);
      
      // Set searchbar to apporpriate size
      setTimeout(function() {
        checkGlobalSearchField();
      }, 100);
    }
  });

  $('.btn-search').blur(function() {
    if (!isMobile) {
      setTimeout(function() {
        if (!$('.search.form-control').is(":focus")) {
          checkGlobalSearchField();
        }
      }, 100);
    }
  });

  // Activate search button when search field has value
  $('.search.form-control').on('input', function() {
    if (!isMobile) {
      if ($(this).val() !== '') {
        $(this).parent().find('.btn-search').addClass('active');
      } else {
        $(this).parent().find('.btn-search').removeClass('active');
      }
    }
  });



  // Do stuff when you click on the mobile-search-toggle btn
  $('.mobile-search-toggle').click(function() {
    paddingSet = 0;
    // Set the search form height to fill the screen
    searchForm.height($(window).height() - navBar.height());

    // Close the nav-menu dropdown if open
    if ($('.nav-menu').hasClass('in')) {
      $('.nav-menu-toggle').click();
    }

    if (readyToToggle) {
      searchForm.slideToggle(300);
      readyToToggle = 0;
    }

    // Setup the search selector items to be centered on the active item
    setTimeout(function() {

      selectorWidth = selector.outerWidth();
      selectorCenter = selectorWidth / 2;
      setScrollPadding();
      searchActiveIndicatorSize();

      // Gather the width of all the items to use for centering
      selector.children().each(function(i) {
        selectorItems.push($(this).width());
      });

      // Once scrolling has ended, detect which item will become active
      // then center it
      $(selector).scrollEnd(function(){
        var closest;
        var closestOrder;
        var closestWidth = 0;
        
        closestOffset = 0;

        selector.children().each(function(i) {
          var leftPosition = Math.abs($(this).position().left);
          
          if (leftPosition < selectorCenter) {
            closest = $(this);
            closestOrder = i;
            selector.children().removeClass('active');
            closest.addClass('active');
          }
        });
        
        setScrollPosition(closestOrder);

        scrollToSearchItem(closestOffset);
      }, 200);

      // Center and activate when an item is clicked
      selector.children().click(function() {
        closestOffset = 0;
        setScrollPosition($(this).index())
        scrollToSearchItem(closestOffset);
      });

        
      function setScrollPosition(index) {
        if (index === 0) {
          closestOffset = 0;
        } else {
          $(selectorItems).each(function(i, val) {
            if (i == 0 || i == index) {
              closestOffset += Math.floor(val / 2);
              closestOffset += 14;
              // Add a bit more to make sure the very 
              // last item is scrolled far enough to be centered
              if (i == selectorItems.length - 1) {
                closestOffset += 100;
              }
            } else if (  i < index) {
              closestOffset += 2 * Math.floor(val / 2);
              closestOffset += 28;
            }
          });
        }
      }
      
      function scrollToSearchItem(elem) {
        searchActiveIndicatorSize();
        selector.animate({
          scrollLeft: 2 * Math.floor(elem / 2)
        }, function() {
        });
      }
    }, 50);

  });
  
  // Make the active indicator line the size of the active item
  function searchActiveIndicatorSize() {
    var active = $('.search-selector .dropdown-menu .active');
    var line = $('.search-selector .mobile-search-active-indicator');
    line.width(active.width()).css('margin-left', -(active.width() / 2) + 1);
  }

  // give the first and last items enough padding to center them
  function setScrollPadding() {
    if (!paddingSet) {
      selector.children('li:first-child').css('padding-left', selectorCenter - selector.children('li:first-child').width() / 2);
      selector.children('li:last-child').css('padding-right', selectorCenter - selector.children('li:last-child').width() / 2 - 16);
      paddingSet = 1;
    }
  }

  // Close the search menu if open
  $('.nav-menu-toggle').click(function() {
    if ($('.search-group').hasClass('in')) {
      $('.mobile-search-toggle').click();
    }
  });

  // Prevent body from scrolling when
  // toggle menu is open
  $('.navbar-collapse').on('show.bs.collapse', function () {
    $('body').addClass('no-scroll');
    setTimeout(function() {
      readyToToggle = 1;
    }, 500);
  }).on('hide.bs.collapse', function () {
    $('body').removeClass('no-scroll');
    setTimeout(function() {
      readyToToggle = 1;
    }, 500);
  });



});
