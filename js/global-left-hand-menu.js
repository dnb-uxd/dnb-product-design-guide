/**
 * dnb-product-design-guide v1.0.0 (http://productdesign.dnb.com/)
 * @license ISC
 */
/* ========================================================================
 * D&B Product Styles: global-left-hand-menu.js
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  'use strict';

  var leftHandMenu = $('.dnb-left-hand-menu');

  // Do stuff when the window resizes
  $(window).resize(function() {
    setTimeout(function() {
      dnbLeftHandMenu();
    }, 100);
  });

  leftHandMenu.on('click', '.accordion-toggle', function(e) {
    e.preventDefault();

    if (!$(this).parent().hasClass('open')) {
      $(this).parent().addClass('open manual');
      $(this).next().slideDown(400, function() {
      });
    } else {
      $(this).next().slideUp(400, function() {
        $(this).parent().removeClass('open manual');
      });
    }
  });


  if ($('body').hasClass('has-sidebar') && !$('body').hasClass('header-fixed')) {
    $('.dnb-left-hand-menu').affix({
      offset: {
        top: $('.navbar-default').outerHeight()
      }
    });
    $('.dnb-left-hand-menu').on("affixed.bs.affix affixed-top.bs.affix", function() {
      dnbLeftHandMenu();
    });


  }

  // Set initial position of Left-hand menu
  dnbLeftHandMenu();

  function dnbLeftHandMenu() {
    var leftMenu = $('.dnb-left-hand-menu');
    var mainNavbar = $('.navbar-default');
    if (leftMenu.hasClass('affix') || leftMenu.hasClass('affix-bottom')) {
      leftMenu.css('top', 0);
    } else {
      leftMenu.css('top', mainNavbar.outerHeight());
    }
  }

});
