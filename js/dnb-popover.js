/**
 * dnb-product-design-guide v1.0.0 (http://productdesign.dnb.com/)
 * @license ISC
 */
/* ========================================================================
 * D&B Product Styles: dnb-popover.js
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  'use strict';
  
  // Add class to body to help with closing popovers
  $('[data-toggle="popover"]').on('shown.bs.popover', function() {
    $('body').addClass('popover-present');
  });

  // Open manually triggerd popovers
  $('[data-toggle="popover"][data-trigger="manual"]').click(function() {
    $(this).popover('show');
  });

  // Close popover with 'close' btn
  $('body').on('click','.popover .close', function(e) {
    var id = $(this).parents('.popover').attr('id');
    $('[aria-describedby="' + id + '"]').popover('hide');
    $('body').removeClass('popover-present');
  });

  // Dismiss any open popup when you click outside of it.
  $('html').on('click', '.popover-present', function (e) {
    $('[data-toggle="popover"]').each(function () {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
        $('body').removeClass('popover-present');
      } else if ($(this).is(e.target)) {
        $(this).popover('hide');
        setTimeout(function() {
          $('body').removeClass('popover-present');
        }, 150);
      }
    });
  });
});