/**
 * dnb-product-design-guide v1.0.0 (http://productdesign.dnb.com/)
 * @license ISC
 */
/* ========================================================================
 * D&B Product Styles: input-file.js
 * http://productdesign.dnb.com/components/
 * ======================================================================== */

jQuery(function($) {
  'use strict';
  
  if ($('.input-file').length) {
    var inputFile = $('.input-file input[type="file"]');

    // Reset input file elements
    inputFile.wrap('<form>').closest('form').get(0).reset();
    inputFile.unwrap();
    $(".fileuploadurl").val(inputFile.val())

    // Update the file name
    inputFile.on('change', function () {
      $(this).parents('.input-file').find(".fileuploadurl").val($(this).val());
    });
  }
});